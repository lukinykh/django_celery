from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

import debug_toolbar

from django_celery_project.settings import DEBUG

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("django_celery_app.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
if DEBUG:
    urlpatterns += [
        path("__debug__/", include(debug_toolbar.urls)),
    ]

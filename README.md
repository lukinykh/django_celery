# django_celery

# Customization

Add user to docker group (start docker-compose without ROOT)

    sudo gpasswd -a $USER docker

For MacOS

    sudo dseditgroup -o edit -a $USER -t user docker

Change owner to user from ROOT after local changes

    sudo chown -R $USER:$USER .


Create Project

    docker-compose run runserver django-admin startproject django_celery_project .


Create Application (after app directory creating)

    docker-compose run runserver django-admin startapp django_celery_app


Make migrations

    docker-compose run runserver sh -c "python manage.py makemigrations"


Remove all unused containers, networks, images (both dangling and unreferenced), and optionally, volumes (carefully).

    docker system prune --all


# Docker Compose launch (in root project directory)

For launch must be installed docker and docker-compose

    docker-compose up runserver

# API requests examples:

Page list API

    http http://0.0.0.0:8000/pages/

Output:

    HTTP/1.1 200 OK
    Allow: GET, POST, HEAD, OPTIONS
    Content-Length: 368
    Content-Type: application/json
    Date: Thu, 30 Sep 2021 06:57:18 GMT
    Referrer-Policy: same-origin
    Server: WSGIServer/0.2 CPython/3.9.7
    Server-Timing: TimerPanel_utime;dur=25.743000000000293;desc="User CPU time", TimerPanel_stime;dur=0.9199999999998099;desc="System CPU time", TimerPanel_total;dur=26.663000000000103;desc="Total CPU time", TimerPanel_total_time;dur=30.350685119628906;desc="Elapsed time", SQLPanel_sql_time;dur=1.043558120727539;desc="SQL 2 queries", CachePanel_total_time;dur=0;desc="Cache 0 Calls"
    Vary: Accept, Cookie
    X-Content-Type-Options: nosniff
    X-Frame-Options: DENY
    
    {
        "count": 7,
        "next": "http://0.0.0.0:8000/pages/?limit=4&offset=4",
        "previous": null,
        "results": [
            {
                "detail_url": "http://0.0.0.0:8000/pages/1/",
                "id": 1,
                "title": "Page1"
            },
            {
                "detail_url": "http://0.0.0.0:8000/pages/2/",
                "id": 2,
                "title": "Page2"
            },
            {
                "detail_url": "http://0.0.0.0:8000/pages/3/",
                "id": 3,
                "title": "Page3"
            },
            {
                "detail_url": "http://0.0.0.0:8000/pages/4/",
                "id": 4,
                "title": "Page4"
            }
        ]
    }


Page detail API

    http http://0.0.0.0:8000/pages/1/

Output:

    HTTP/1.1 200 OK
    Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS
    Content-Length: 1101
    Content-Type: application/json
    Date: Thu, 30 Sep 2021 06:58:30 GMT
    Referrer-Policy: same-origin
    Server: WSGIServer/0.2 CPython/3.9.7
    Server-Timing: TimerPanel_utime;dur=80.27899999999732;desc="User CPU time", TimerPanel_stime;dur=14.486000000000665;desc="System CPU time", TimerPanel_total;dur=94.764999999998;desc="Total CPU time", TimerPanel_total_time;dur=257.22765922546387;desc="Elapsed time", SQLPanel_sql_time;dur=7.151603698730469;desc="SQL 3 queries", CachePanel_total_time;dur=0;desc="Cache 0 Calls"
    Vary: Accept, Cookie
    X-Content-Type-Options: nosniff
    X-Frame-Options: DENY
    
    [
        {
            "text": {
                "body": "Django REST framework is a powerful and flexible toolkit for building Web APIs.",
                "counter": 2,
                "id": 1,
                "page": 1,
                "title": "Text title1"
            }
        },
        {
            "text": {
                "body": "Some reasons you might want to use REST framework:\r\n\r\n    The Web browsable API is a huge usability win for your developers.\r\n    Authentication policies including packages for OAuth1a and OAuth2.\r\n    Serialization that supports both ORM and non-ORM data sources.\r\n    Customizable all the way down - just use regular function-based views if you don't need the more powerful features.\r\n    Extensive documentation, and great community support.\r\n    Used and trusted by internationally recognised companies including Mozilla, Red Hat, Heroku, and Eventbrite.",
                "counter": 2,
                "id": 4,
                "page": 1,
                "title": "Text title2"
            }
        },
        {
            "audio": {
                "bitrate": 320,
                "counter": 2,
                "id": 2,
                "page": 1,
                "title": "Apashe - Majesty.mp3"
            }
        },
        {
            "video": {
                "counter": 2,
                "id": 3,
                "page": 1,
                "subtitles_url": "https://www.youtube.com/watch?v=8WqBRBogw1s/subs",
                "title": "New York World Trade Center Area 1999 Unedited",
                "video_url": "https://www.youtube.com/watch?v=8WqBRBogw1s"
            }
        }
    ]

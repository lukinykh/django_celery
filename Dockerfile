# syntax=docker/dockerfile:1
FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /django_celery
RUN pip install --upgrade pip
COPY requirements.txt /django_celery/
RUN pip install -r requirements.txt
COPY . /django_celery/

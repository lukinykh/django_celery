import time

from django.db import connection
from django.db.utils import ConnectionRouter, OperationalError
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    """Django command that waits for database to be available"""

    def handle(self, *args, **options):
        """Handle the command"""
        self.stdout.write("Waiting for database...")
        db_conn = None
        counter = 0
        while not db_conn:
            try:
                connection.ensure_connection()
                db_conn = True
            except OperationalError:
                self.stdout.write("Database unavailable, waiting 1 second...")
                time.sleep(1)
            counter += 1
            if counter == 10:
                raise ConnectionError("Database not available! Check your settings..")

        self.stdout.write(self.style.SUCCESS("Database available!"))

from django.db import models


class Page(models.Model):
    title = models.CharField(max_length=80)

    def __str__(self):
        return self.title


class Content(models.Model):
    title = models.CharField(max_length=80)
    counter = models.PositiveIntegerField(default=0)
    page = models.ForeignKey(Page, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Text(Content):
    body = models.TextField(blank=True)


class Video(Content):
    video_url = models.URLField(blank=True)
    subtitles_url = models.URLField(blank=True)


class Audio(Content):
    BITRATE_CHOICES = [
        (32, "32 Kbps"),
        (96, "96 Kbps"),
        (128, "128 Kbps"),
        (192, "192 Kbps"),
        (256, "256 Kbps"),
        (320, "320 Kbps"),
    ]
    bitrate = models.IntegerField(
        choices=BITRATE_CHOICES,
        default=128,
    )

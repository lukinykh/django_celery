from django.contrib import admin

from django_celery_app.models import Page, Content, Audio, Video, Text


class ContentInline(admin.TabularInline):
    exclude = ("counter",)
    model = Content
    extra = 0


@admin.register(Text)
class TextAdmin(admin.ModelAdmin):
    exclude = ("counter",)
    list_display = ("title", "body")


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    exclude = ("counter",)
    list_display = ("title", "video_url", "subtitles_url")


@admin.register(Audio)
class AudioAdmin(admin.ModelAdmin):
    exclude = ("counter",)
    list_display = ("title", "bitrate")


class PageAdmin(admin.ModelAdmin):
    inlines = [
        ContentInline,
    ]
    search_fields = [
        "title",
        "content__text__body",
        "content__video__video_url",
        "content__audio__title",
    ]


admin.site.register(Page, PageAdmin)

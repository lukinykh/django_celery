from collections import OrderedDict
from rest_framework import serializers

from django_celery_app.models import Page, Audio, Video, Text


class PageListSerializer(serializers.ModelSerializer):
    detail_url = serializers.HyperlinkedIdentityField(view_name="page-detail")

    class Meta:
        model = Page
        fields = [
            "id",
            "title",
            "detail_url",
        ]


class AudioSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Audio


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Video


class TextSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Text


class PageDetailSerializer(serializers.ModelSerializer):
    audio = AudioSerializer()
    video = VideoSerializer()
    text = TextSerializer()

    def to_representation(self, instance):
        result = super().to_representation(instance)
        return OrderedDict(
            [(key, result[key]) for key in result if result[key] is not None]
        )

    class Meta:
        fields = ["audio", "video", "text"]
        model = Page

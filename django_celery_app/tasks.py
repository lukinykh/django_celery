from celery import shared_task
from django.db import transaction
from django.db.models import F

from django_celery_app.models import Content


@shared_task
def save_async(pk):
    with transaction.atomic():
        Content.objects.filter(page__id=pk).update(counter=F("counter") + 1)

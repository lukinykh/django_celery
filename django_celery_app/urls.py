from rest_framework.routers import DefaultRouter

from django_celery_app import views

router = DefaultRouter()
router.register(r"pages", views.PageViewSet, basename="page")
urlpatterns = router.urls

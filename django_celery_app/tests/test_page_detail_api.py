import pytest
from django.urls import reverse

from django_celery_app.models import Page, Text, Video, Audio


@pytest.mark.django_db
def test_page_detail(api_client):
    """Testing detail page params and check status code"""
    Page.objects.create(title='title')
    Text.objects.create(page_id=1, title='Article', body='some text in body')
    Audio.objects.create(page_id=1, title='Song', bitrate=128)
    Video.objects.create(page_id=1, title='Movie',
                         video_url='https://youtube.com',
                         subtitles_url='https://youtube.com/subs')

    url = reverse("page-detail", kwargs={"pk": 1})
    response = api_client.get(url, format="json")
    correct_resp = [{'video': {'id': 3,
                               'title': 'Movie',
                               'counter': 0,
                               'video_url': 'https://youtube.com',
                               'subtitles_url': 'https://youtube.com/subs',
                               'page': 1}},
                    {'audio': {'id': 2,
                               'title': 'Song',
                               'counter': 0,
                               'bitrate': 128,
                               'page': 1}},
                    {'text': {'id': 1,
                              'title': 'Article',
                              'counter': 0,
                              'body': 'some text in body',
                              'page': 1}},
                    ]
    assert response.status_code == 200, "Get incorrect status code"
    assert response.json() == correct_resp, "Incorrect response data"

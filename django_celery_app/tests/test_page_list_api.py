import pytest
from django.urls import reverse

from django_celery_app.models import Page


@pytest.mark.django_db
def test_pages_count(api_client):
    """Testing total count of API pages and check status code"""
    Page.objects.bulk_create([
        Page(title='Page1'),
        Page(title='Page2'),
        Page(title='Page3'),
        Page(title='Page4'),
    ])
    correct_resp = {'count': 4,
                    'next': None,
                    'previous': None,
                    'results': [{'id': 2,
                                 'title': 'Page1',
                                 'detail_url': 'http://testserver/pages/2/'},
                                {'id': 3, 'title': 'Page2',
                                 'detail_url': 'http://testserver/pages/3/'},
                                {'id': 4, 'title': 'Page3',
                                 'detail_url': 'http://testserver/pages/4/'},
                                {'id': 5, 'title': 'Page4',
                                 'detail_url': 'http://testserver/pages/5/'}]}

    url = reverse("page-list")
    response = api_client.get(url, format="json")
    assert response.status_code == 200, "Get incorrect status code"
    assert response.json() == correct_resp, "Incorrect response data"


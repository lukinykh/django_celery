from rest_framework import viewsets
from rest_framework.filters import OrderingFilter
from rest_framework.response import Response

from django_celery_app import tasks
from django_celery_app.models import Page
from django_celery_app.serializers import PageListSerializer, PageDetailSerializer


class PageViewSet(viewsets.ModelViewSet):
    """Getting list of Page URLs"""

    serializer_class = PageListSerializer
    queryset = Page.objects.all()
    filter_backends = [OrderingFilter]
    ordering_fields = "__all__"

    def retrieve(self, request, pk=None):
        data = (
            Page.objects.get(id=pk)
            .content_set.all()
            .select_related("text", "video", "audio")
        )
        tasks.save_async.delay(pk)
        serializer = PageDetailSerializer(instance=data, many=True)
        return Response(serializer.data)
